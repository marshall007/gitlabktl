# GitLab Knative / Kubernetes CLI

GitLab Knative / Kubernetes CLI to manage your:

- serverless functions
- serverless applications

## Usage

See `gitlabktl --help`

1. Bulding a function runtime

    ```
    gitlabktl function build --help
    ```
    
1. Build functions defined in a serverless configuration file

    ```
    gitlabktl serverless build --help
    ```

1. Deploy functions defined in a serverless configuration file

    ```
    gitlabktl serverless deploy --help
    ```
    
1. Build a serverless application using a `Dockerfile`

    ```
    gitlabktl app build --help
    ```
    
## State

Alpha

## License

MIT
