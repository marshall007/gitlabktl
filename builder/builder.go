package builder

import (
	"context"

	"gitlab.com/gitlab-org/gitlabktl/builder/kaniko"
	"gitlab.com/gitlab-org/gitlabktl/registry"
	"gitlab.com/gitlab-org/gitlabktl/runtime"
)

// Request describes what a builder needs to build
type Request struct {
	Dockerfile  string
	Directory   string
	Destination string
	Registry    registry.Registry
}

// Builder interface, currently Kaniko, Docker Engine later
type Builder interface {
	DryRun() error
	Build(ctx context.Context) error
}

// Create a new Builder from a function runtime
func NewFromRuntime(runtime runtime.Runtime) Builder {
	return kaniko.NewFromRuntime(runtime)
}

// Create a ne Builder from a Builder Request
func NewFromRequest(request Request) Builder {
	return &kaniko.Kaniko{
		Dockerpath:  request.Dockerfile,
		Workspace:   request.Directory,
		Destination: request.Destination,
		Registry:    request.Registry,
	}
}
