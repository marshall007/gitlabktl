package kaniko

import (
	"bytes"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

func newKaniko() Kaniko {
	return Kaniko{
		Dockerpath:  "Dockerfile.tmp",
		Dockerfile:  "FROM scratch",
		Workspace:   "build/",
		Destination: "my-image",
		Registry: registry.Registry{
			Host: "registry",
			Credentials: registry.Credentials{
				Username: "my-user",
				Password: "my-pass",
			},
		},
	}
}

func TestExecutorCommand(t *testing.T) {
	kaniko := newKaniko()

	command := kaniko.commandToString()
	expected := "/kaniko/executor --context build/ --dockerfile Dockerfile.tmp --destination my-image --cleanup"

	assert.Equal(t, expected, command)
}

func TestDryRun(t *testing.T) {
	buffer := new(bytes.Buffer)

	oldOutput := logrus.StandardLogger().Out
	logrus.SetOutput(buffer)
	defer logrus.SetOutput(oldOutput)

	kaniko := newKaniko()
	err := kaniko.DryRun()
	assert.NoError(t, err)
	output := buffer.String()

	assert.Contains(t, output, "/kaniko/executor")
	assert.Contains(t, output, "FROM scratch")
}

func TestNewWriteDockerfile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		kaniko.writeDockerfile()
		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "FROM scratch", string(output))
	})
}

func TestExistingWriteDockerfile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		err := fs.WriteFile(kaniko.Dockerpath, []byte("stub"), 664)
		require.NoError(t, err)

		kaniko.writeDockerfile()
		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "stub", string(output))
	})
}

func TestNewWriteAuthFile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		kaniko.writeAuthFile()
		output, err := fs.ReadFile(authfile)
		require.NoError(t, err)

		assert.Contains(t, string(output), "auths")
	})
}

func TestExistingWriteAuthFile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		err := fs.WriteFile(authfile, []byte("stub"), 664)
		require.NoError(t, err)

		kaniko.writeAuthFile()
		output, err := fs.ReadFile(authfile)
		require.NoError(t, err)

		assert.Contains(t, string(output), "auths")
	})
}

func TestDockerfileWrite(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()

		kaniko.writeDockerfile()
		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "FROM scratch", string(output))
	})
}

func TestNoDockerfileContents(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := Kaniko{
			Dockerpath:  "Dockerfile.tmp",
			Workspace:   "build/",
			Destination: "my-image",
		}

		err := fs.WriteFile(kaniko.Dockerpath, []byte("FROM ruby"), 664)
		require.NoError(t, err)

		kaniko.writeDockerfile()

		output, err := fs.ReadFile(kaniko.Dockerpath)
		require.NoError(t, err)

		assert.Equal(t, "FROM ruby", string(output))
	})
}

func TestNoDockerfile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := Kaniko{
			Dockerpath:  "Dockerfile.tmp",
			Workspace:   "build/",
			Destination: "my-image",
		}

		assert.Panics(t, func() { kaniko.writeDockerfile() })
	})
}
