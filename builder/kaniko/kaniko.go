// Package kaniko is responsible for performing a Kaniko build of an image.
// Kaniko builder does not require Docker Engine presence, therefore it is a
// better solution one can using within Continuous Integration context.
package kaniko

import (
	"context"
	"os"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/registry"
	"gitlab.com/gitlab-org/gitlabktl/runtime"
)

type Kaniko struct {
	Dockerpath  string
	Dockerfile  string
	Workspace   string
	Destination string
	Registry    registry.Registry
}

const (
	executor = "/kaniko/executor"
	authfile = "/kaniko/.docker/config.json"
)

func NewFromRuntime(runtime runtime.Runtime) *Kaniko {
	return &Kaniko{
		Dockerpath:  "Dockerfile." + runtime.Slug(),
		Dockerfile:  runtime.Dockerfile(),
		Workspace:   runtime.CodeDirectory,
		Destination: runtime.ResultingImage,
		Registry:    registry.NewWithPushAccess(),
	}
}

func (kaniko *Kaniko) Build(ctx context.Context) error {
	logrus.Info("building runtime image using Kaniko")

	kaniko.writeAuthFile()
	kaniko.writeDockerfile()

	logrus.WithField("command", kaniko.commandToString()).Info("executing command")
	err := kaniko.executorCmd(ctx).Run()

	if err != nil {
		logrus.WithError(err).Warn("could not execute Kaniko build")
	}

	return err
}

func (kaniko *Kaniko) DryRun() error {
	logrus.Info("building runtime image using Kaniko (dry-run!)")
	logrus.WithField(kaniko.Dockerpath, kaniko.Dockerfile).Info("using Dockerfile")
	logrus.WithField(authfile, kaniko.authFileContents()).Debug("using auths config.json")
	logrus.WithField("command", kaniko.commandToString()).Info("using command")

	return nil
}

func (kaniko *Kaniko) executorCmd(ctx context.Context) *exec.Cmd {
	command := exec.CommandContext(ctx, executor,
		"--context", kaniko.Workspace,
		"--dockerfile", kaniko.Dockerpath,
		"--destination", kaniko.Destination,
		"--cleanup",
	)

	command.Stdout = os.Stdout
	command.Stderr = os.Stderr

	return command
}

func (kaniko *Kaniko) commandToString() string {
	cmd := kaniko.executorCmd(context.Background())

	return strings.Join(cmd.Args, " ")
}

func (kaniko *Kaniko) writeAuthFile() {
	if exists, _ := fs.Exists(authfile); exists {
		logrus.WithField("path", authfile).Warn("file already exists, overwriting")
	}

	writeToFile(authfile, []byte(kaniko.authFileContents()))
}

func (kaniko *Kaniko) authFileContents() string {
	contents, err := kaniko.Registry.ToAuthFileContents()

	if err != nil {
		logrus.WithError(err).Fatal("could not generate docker auth file")
	}

	return contents
}

func (kaniko *Kaniko) writeDockerfile() {
	exists, err := fs.Exists(kaniko.Dockerpath)
	contents := len(kaniko.Dockerfile) > 0

	switch {
	case err != nil:
		logrus.WithError(err).Fatal("unknown filesystem state")
	case exists && contents:
		logrus.WithField("path", kaniko.Dockerpath).Warn("file already exists, skipping overwrite")
	case !exists && !contents:
		panic("Dockerfile not found and no Dockerfile contents available")
	case !exists && contents:
		writeToFile(kaniko.Dockerpath, []byte(kaniko.Dockerfile))
	}
}

func writeToFile(path string, data []byte) {
	file, createError := fs.Create(path)

	if createError != nil {
		logrus.WithField("path", path).WithError(createError).Fatal("could not open file")
	}

	defer file.Close()
	_, writeError := file.Write(data)

	if writeError != nil {
		logrus.WithField("path", path).WithError(writeError).Fatal("could not write to file")
	}
}
