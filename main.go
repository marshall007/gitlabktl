package main

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/gitlabktl/app"
)

func main() {
	fmt.Println("Welcome to GitLab CLI tool")

	app := app.NewApp()
	app.RegisterCommands()
	err := app.Run(os.Args)

	if err != nil {
		logrus.Fatal(err)
	}
}
