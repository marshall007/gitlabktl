package serverless

import (
	"path"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"github.com/triggermesh/tm/pkg/client"
	"github.com/triggermesh/tm/pkg/resources/credential"
	"github.com/triggermesh/tm/pkg/resources/service"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/config"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

// DeployAction struct is going to be populated with CLI invocation arguments.
type DeployAction struct {
	DryRun     bool   `short:"t" long:"dry-run" env:"GITLAB_DRYRUN" description:"Dry run only"`
	ConfigFile string `short:"f" long:"file" env:"GITLAB_SERVERLESS_FILE" description:"Path to serverless manifest file"`
	KubeConfig string `short:"c" long:"kubeconfig" env:"GITLAB_KUBECONFIG_FILE" description:"Path to kubeconfig"`
}

func (action *DeployAction) Execute(context *commands.Context) error {
	client.Namespace = action.namespace()
	client.Dry = action.DryRun
	client.Wait = true

	clientset, err := client.NewClient(action.kubeconfig())

	if err != nil {
		logrus.WithError(err).Fatal("could not load kubernetes config")
	}

	err = action.deployCredentials(&clientset)

	if err != nil {
		logrus.WithError(err).Fatal("could not deploy registry credentials")
	}

	deployment := &service.Service{
		Namespace: client.Namespace,
		Registry:  client.Registry,
	}

	functions := action.functionsServices(deployment)

	return deployment.DeployFunctions(functions, true, 3, &clientset)
}

// Temporary mechanism for deploying registry credentials.
func (action *DeployAction) deployCredentials(clientset *client.ConfigSet) error {
	// `tm` tool does not respect dry run when deploying credentials
	if action.DryRun {
		return nil
	}

	registry := registry.NewWithPullAccess()

	logrus.Info("deploying registry credentials")

	credentials := credential.RegistryCreds{
		Name:      "gitlab-registry",
		Namespace: client.Namespace,
		Host:      registry.Host,
		Username:  registry.Username,
		Password:  registry.Password,
		Pull:      true,
	}

	return credentials.CreateRegistryCreds(clientset)
}

func (action *DeployAction) namespace() string {
	return env.Getenv("KUBE_NAMESPACE")
}

func (action *DeployAction) kubeconfig() string {
	if len(action.KubeConfig) > 0 {
		path, _ := filepath.Abs(action.KubeConfig)
		return path
	}

	return env.Getenv("KUBECONFIG")
}

func (action *DeployAction) manifest() string {
	if len(action.ConfigFile) > 0 {
		return action.ConfigFile
	}

	return config.ServerlessFile
}

func (action *DeployAction) functionsServices(deployment *service.Service) (functions []service.Service) {
	funcs, err := deployment.ManifestToServices(action.manifest())

	if err != nil {
		logrus.WithError(err).Fatal("could not parse serverless functions")
	}

	for _, function := range funcs {
		if len(deployment.Registry) > 0 {
			function.Source = path.Join(deployment.Registry, function.Name)
		} else {
			function.Source = path.Join(registry.DefaultRepository(), function.Name)
		}

		function.Buildtemplate = ""
		functions = append(functions, function)
	}

	return functions
}

func newDeployCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "serverless/deploy",
			Config: commands.Config{
				Name:        "deploy",
				Aliases:     []string{},
				Usage:       "Deploy your serverless services",
				Description: "This commands deploys your images to Knative cluster",
			},
		},
		Handler: new(DeployAction),
	}
}

func RegisterDeployCommand(register *commands.Register) {
	register.RegisterCommand(newDeployCommand())
}
