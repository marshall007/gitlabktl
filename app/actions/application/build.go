package application

import (
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/builder"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

type buildAction struct {
	Dockerfile string `long:"dockerfile" env:"GITLAB_APP_DOCKERFILE" description:"Dockerfile path"`
	Directory  string `long:"directory" env:"GITLAB_APP_DIRECTORY" description:"Directory with code"`
	Image      string `long:"image" env:"GITLAB_APP_IMAGE" description:"Resulting image repository"`
	Tag        string `long:"tag" env:"GITLAB_APP_TAG" description:"Resulting image tag"`
	Username   string `long:"registry-username" env:"GITLAB_REGISTRY_USERNAME" description:"Registry username"`
	Password   string `long:"registry-password" env:"GITLAB_REGISTRY_PASSWORD" description:"Registry password"`
	DryRun     bool   `short:"t" long:"dry-run" env:"GITLAB_APP_DRYRUN" description:"Dry run only"`
}

func (action *buildAction) Execute(context *commands.Context) error {
	builder := builder.NewFromRequest(builder.Request{
		Dockerfile:  action.SourceDockerfile(),
		Directory:   action.SourceContext(),
		Destination: action.DestinationImage(),
		Registry:    action.DestinationRegistry(),
	})

	if action.DryRun {
		return builder.DryRun()
	}

	return builder.Build(context.Ctx)
}

func (action *buildAction) SourceDockerfile() string {
	if len(action.Dockerfile) > 0 {
		return action.Dockerfile
	}

	return "Dockerfile"
}

func (action *buildAction) SourceContext() string {
	pwd, err := os.Getwd()
	if err != nil {
		logrus.WithError(err).Fatal("could not read current directory")
	}

	if len(action.Directory) == 0 {
		return pwd
	}

	context, err := filepath.Abs(action.Directory)
	if err != nil {
		logrus.WithError(err).Fatal("could not calculate absolute context path")
	}

	return context
}

func (action *buildAction) DestinationRepository() string {
	if len(action.Image) > 0 {
		return action.Image
	}

	return env.Getenv("CI_REGISTRY_IMAGE")
}

func (action *buildAction) DestinationImage() string {
	repository := action.DestinationRepository()

	if len(repository) == 0 {
		panic("unable to determine image destination repository")
	}

	if len(action.Tag) > 0 {
		return strings.Join([]string{repository, action.Tag}, ":")
	}

	return strings.Join([]string{repository, "latest"}, ":")
}

func (action *buildAction) DestinationRegistry() registry.Registry {
	if len(action.Image) == 0 {
		return registry.NewWithPushAccess()
	}

	address, err := url.Parse("http://" + action.DestinationRepository())

	if err != nil {
		logrus.WithError(err).Fatal("could not parse registry image repository location")
	} else {
		logrus.WithField("registry", address.Host).Info("using registry destination")
	}

	if len(address.Host) == 0 {
		logrus.Fatal("could not extract registry location")
	}

	return registry.Registry{
		Host: address.Host,
		Credentials: registry.Credentials{
			Username: action.Username,
			Password: action.Password,
		},
	}
}

func newBuildCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "application/build",
			Config: commands.Config{
				Name:        "build",
				Aliases:     []string{},
				Usage:       "Build your serverless application",
				Description: "This commands builds an image using a Dockerfile",
			},
		},
		Handler: new(buildAction),
	}
}

func RegisterBuildCommand(register *commands.Register) {
	register.RegisterCommand(newBuildCommand())
}
