package application

import (
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
)

func TestActionSourceDockerfile(t *testing.T) {
	action := buildAction{Dockerfile: "Dockerfile.app"}
	assert.Equal(t, "Dockerfile.app", action.SourceDockerfile())

	action = buildAction{}
	assert.Equal(t, "Dockerfile", action.SourceDockerfile())
}

func TestActionSourceContext(t *testing.T) {
	pwd, err := os.Getwd()
	require.NoError(t, err)

	action := buildAction{Directory: "my/directory"}
	assert.Equal(t, path.Join(pwd, "my/directory"), action.SourceContext())

	action = buildAction{Directory: "/var/my/directory"}
	assert.Equal(t, "/var/my/directory", action.SourceContext())

	action = buildAction{}
	assert.Equal(t, pwd, action.SourceContext())
}

func TestDestinationRepository(t *testing.T) {
	action := buildAction{Image: "registry.example/my/repository"}
	assert.Equal(t, "registry.example/my/repository", action.DestinationRepository())

	env.WithStubbedEnv(env.Stubs{"CI_REGISTRY_IMAGE": "my.registry/project"}, func() {
		action = buildAction{}

		assert.Equal(t, "my.registry/project", action.DestinationRepository())
	})
}

func TestActionCustomDestinationRegistry(t *testing.T) {
	action := buildAction{
		Image:    "registry.example/my/repository",
		Username: "my-user",
	}

	registry := action.DestinationRegistry()

	assert.Equal(t, "registry.example", registry.Host)
	assert.Equal(t, "my-user", registry.Username)
	assert.Empty(t, registry.Password)
}

func TestActionDefaultDestinationRegistry(t *testing.T) {
	action := buildAction{}

	environment := env.Stubs{
		"CI_REGISTRY":          "my.registry",
		"CI_REGISTRY_IMAGE":    "my.registry/project",
		"CI_REGISTRY_USER":     "my-username",
		"CI_REGISTRY_PASSWORD": "my-password",
	}

	env.WithStubbedEnv(environment, func() {
		registry := action.DestinationRegistry()

		assert.Equal(t, "my.registry", registry.Host)
		assert.Equal(t, "my-username", registry.Username)
		assert.Equal(t, "my-password", registry.Password)
	})
}

func TestActionDestinationImage(t *testing.T) {
	action := buildAction{Image: "registry.example/project"}

	assert.Equal(t, "registry.example/project:latest", action.DestinationImage())

	action = buildAction{Image: "registry.example/project", Tag: "testing"}

	assert.Equal(t, "registry.example/project:testing", action.DestinationImage())
}

func TestDefaultDestinationImage(t *testing.T) {
	environment := env.Stubs{"CI_REGISTRY_IMAGE": "my.registry/project"}

	env.WithStubbedEnv(environment, func() {
		action := buildAction{}

		assert.Equal(t, "my.registry/project:latest", action.DestinationImage())
	})
}
