package function

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/builder"
	mocks "gitlab.com/gitlab-org/gitlabktl/mocks/builder"
)

func TestActionToRuntimeDetails(t *testing.T) {
	action := BuildAction{
		Name:      "my-function",
		Image:     "my/resulting/image",
		Runtime:   "https://gitlab.com/gitlab-org/serverless/runtimes/ruby",
		Directory: "lib/",
		Handler:   "MyClass.my_function",
	}

	details := action.toRuntimeDetails()

	assert.Equal(t, action.Runtime, details.RuntimeAddress)
	assert.Equal(t, "my-function", details.FunctionName)
}

func TestDryRunExecute(t *testing.T) {
	withMockBuilder(t, func(mock *mocks.MockBuilder) {
		mock.On("DryRun").Return(nil).Once()

		action := BuildAction{DryRun: true}
		action.Execute(new(commands.Context))
	})
}

func TestBuildExecute(t *testing.T) {
	withMockBuilder(t, func(mock *mocks.MockBuilder) {
		mock.On("Build").Return(nil).Once()

		action := BuildAction{DryRun: false}
		action.Execute(new(commands.Context))
	})
}

func withMockBuilder(t *testing.T, test func(mock *mocks.MockBuilder)) {
	mockBuilder := new(mocks.MockBuilder)
	defer mockBuilder.AssertExpectations(t)

	oldBuilder := newBuilder
	defer func() {
		newBuilder = oldBuilder
	}()

	newBuilder = func(action *BuildAction) builder.Builder {
		return mockBuilder
	}

	test(mockBuilder)
}
