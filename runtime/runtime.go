package runtime

import (
	"path"

	"github.com/gosimple/slug"
	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

var (
	newTemplate = func(details Details) *Template {
		return NewTemplate(details)
	}
)

type Details struct {
	FunctionName    string
	FunctionFile    string
	FunctionHandler string
	RuntimeAddress  string
	CodeDirectory   string
	ResultingImage  string
}

type Runtime struct {
	Details
}

func New(details Details) Runtime {
	return Runtime{Details: details}
}

func (runtime Runtime) Dockerfile() string {
	if len(runtime.RuntimeAddress) > 0 {
		logrus.Info("generating runtime Dockerfile")

		return newTemplate(runtime.Details).Dockerfile()
	}

	dockerpath := path.Join(runtime.CodeDirectory, "Dockerfile")
	output, err := fs.ReadFile(dockerpath)

	if err != nil {
		logrus.WithError(err).Fatal("could not read runtime Dockerfile")
	}

	return string(output)
}

func (runtime Runtime) Slug() string {
	return slug.Make(runtime.FunctionName)
}
