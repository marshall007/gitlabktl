package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"

	mocks "gitlab.com/gitlab-org/gitlabktl/mocks/runtime"
)

func TestDockerfile(t *testing.T) {
	location := Location{
		Address:   "https://gitlab.com/grzesiek/some-runtime",
		Reference: "v0.1alpha1",
	}

	client := new(mocks.MockClient)
	client.On("ReadFile", "my-template").Return("FROM {{.FunctionName}}").Once()
	defer client.AssertExpectations(t)

	repository := Repository{
		Client:   client,
		Location: &location,
	}

	template := Template{
		runtime: Details{
			FunctionName:   "ruby-function",
			RuntimeAddress: "https://gitlab.com/gitlab-org/serverless/runtimes/ruby",
		},
		Filename:   "my-template",
		Repository: &repository,
	}

	assert.Equal(t, "FROM ruby-function", template.Dockerfile())
}
