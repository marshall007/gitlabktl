package runtime

import (
	"github.com/sirupsen/logrus"
	"gopkg.in/src-d/go-billy.v4/memfs"
	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/storage/memory"
)

type Repository struct {
	Client
	*Location
}

type Location struct {
	Address   string
	Reference string
}

type Client interface {
	ReadFile(path string) string
}

type GoGit struct {
	*Location
	*git.Repository
}

func (client *GoGit) ReadFile(path string) string {
	ref, err := client.Head()

	if err != nil {
		logrus.WithError(err).Fatal("could not fetch runtime repository HEAD")
	}

	commit, err := client.CommitObject(ref.Hash())

	if err != nil {
		logrus.WithError(err).Fatal("could not find runtime version commit")
	}

	file, err := commit.File(path)

	if err != nil {
		logrus.WithError(err).Fatal("could not find runtime template")
	}

	contents, err := file.Contents()

	if err != nil {
		logrus.WithError(err).Fatal("could not read runtime template contents")
	}

	return contents
}

func NewRepository(address string) *Repository {
	location := &Location{Address: address, Reference: "master"}

	repository, err := git.Clone(memory.NewStorage(), memfs.New(), &git.CloneOptions{
		URL: location.Address,
	})

	if err != nil {
		logrus.WithError(err).Fatal("failed to clone runtime repository")
	}

	worktree, err := repository.Worktree()

	if err != nil {
		logrus.WithError(err).Fatal("could not fetch the repository worktree")
	}

	err = worktree.Checkout(&git.CheckoutOptions{
		Branch: plumbing.NewBranchReferenceName(location.Reference),
	})

	if err != nil {
		logrus.WithError(err).Fatal("could not checkout the runtime version")
	}

	client := &GoGit{Location: location, Repository: repository}

	return &Repository{Location: location, Client: client}
}
