package builder

import (
	"context"

	"github.com/stretchr/testify/mock"
)

type MockBuilder struct {
	mock.Mock
}

func (client *MockBuilder) Build(ctx context.Context) error {
	args := client.Called()

	return args.Error(0)
}

func (client *MockBuilder) DryRun() error {
	args := client.Called()

	return args.Error(0)
}
