package registry

import (
	"encoding/json"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
)

type Registry struct {
	Host string
	Credentials
}

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (registry Registry) ToAuthFileContents() (string, error) {
	values := make(map[string]map[string]Credentials)
	values["auths"] = make(map[string]Credentials)
	values["auths"][registry.Host] = registry.Credentials

	contents, err := json.Marshal(values)

	if err != nil {
		return "", err
	}

	return string(contents), nil
}

func NewWithPushAccess() Registry {
	if !env.Defined("CI_REGISTRY", "CI_REGISTRY_USER", "CI_REGISTRY_PASSWORD") {
		// Temporary behavior until we refactor logrus with stubbable logger
		panic("no container registry environment detected")
	}

	return Registry{
		Host: env.Getenv("CI_REGISTRY"),
		Credentials: Credentials{
			Username: env.Getenv("CI_REGISTRY_USER"),
			Password: env.Getenv("CI_REGISTRY_PASSWORD"),
		},
	}
}

func NewWithPullAccess() Registry {
	if !env.Defined("CI_REGISTRY", "CI_DEPLOY_USER", "CI_DEPLOY_PASSWORD") {
		// Temporary behavior until we refactor logrus with stubbable logger
		panic("deploy token not found")
	}

	return Registry{
		Host: env.Getenv("CI_REGISTRY"),
		Credentials: Credentials{
			Username: env.Getenv("CI_DEPLOY_USER"),
			Password: env.Getenv("CI_DEPLOY_PASSWORD"),
		},
	}
}

func DefaultRepository() string {
	return env.Getenv("CI_REGISTRY_IMAGE")
}
