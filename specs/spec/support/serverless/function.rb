require_relative '../kubernetes/helpers'
require 'httparty'

module Serverless
  class Function
    extend Kubernetes::Helpers

    def initialize(service)
      @service = service
    end

    def has_domain?
      !@service.domain.empty?
    end

    def post!
      yield HTTParty.post(@service.endpoint)
    end

    def sha
      ENV['CI_COMMIT_SHA']
    end

    def self.find(name)
      (@functions ||= {}).fetch(name) do
        @functions[name] = new(
          knative_services.find { |service| service.name == name }
        )
      end
    end
  end
end
