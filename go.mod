module gitlab.com/gitlab-org/gitlabktl

go 1.12

require (
	github.com/gosimple/slug v1.4.2
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/afero v1.2.1
	github.com/stretchr/testify v1.3.0
	github.com/triggermesh/tm v0.0.11-0.20190328124947-87a194546274
	github.com/urfave/cli v1.20.0
	gitlab.com/ayufan/golang-cli-helpers v0.0.0-20171103152739-a7cf72d604cd
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a // indirect
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	golang.org/x/net v0.0.0-20190320064053-1272bf9dcd53 // indirect
	golang.org/x/sys v0.0.0-20190318195719-6c81ef8f67ca // indirect
	golang.org/x/tools v0.0.0-20190319232107-3f1ed9edd1b4 // indirect
	gopkg.in/src-d/go-billy.v4 v4.3.0
	gopkg.in/src-d/go-git.v4 v4.10.0
	gopkg.in/yaml.v2 v2.2.2
)
