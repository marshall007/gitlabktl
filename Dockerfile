FROM golang:1.12 AS builder
WORKDIR /app
ENV GO111MODULE on
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build

FROM gcr.io/kaniko-project/executor:debug-v0.9.0
COPY --from=builder /app/gitlabktl /usr/bin/gitlabktl
WORKDIR /root
ENTRYPOINT []
